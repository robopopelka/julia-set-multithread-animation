#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>

#include "messages.h"
#include "utils.h"

#ifndef COMPUTATION_H
#define COMPUTATION_H

typedef struct complex_num {
    double real;
    double imag;
} ComplexNum;

void computauion_init(void);
// initialize window parameters and allocate buffer
void computauion_cleanup(void);
// free the buffer
bool is_computing(void);
// return tru if computing
bool is_done(void);
// return true if img done
void get_grid_size(int *w, int *h);
// assign grid width and height to *w and *h
void update_image(int w, int h, unsigned char *img);
// refresh the image window with current buffer
void abort_comp(void);
// abort computation
void enable_comp(void);
// change the abort flag
bool is_abort(void);
// return abort status
bool set_compute(message *msg);
// send computation parameters to NUCLEO
bool compute(message *msg);
// send compute msg to nucleo with current chunk parameters
void reset_chunk(void);
// reset cid and current computation status
void compute_fractal();
// compute fractal on PC
void reset_buffer(int w, int h, unsigned char *img);
// fill buffer with 0s and update image window
void update_data(const msg_compute_data *compute_data);
// fill buffer new computation results
void change_c(void);
// change the C constant
void shift_c(void);
// shift the C constant (for animation)
void reset_c(void);
// reset the C constant to default values
void shift_range(void);
// shift the complex number range (for zoom animation)
void reset_range(void);
// reset the complex number range to default values
void compute_increment(void);
// update the increment of complex values (shift by one pixel) based on range parameters
#endif