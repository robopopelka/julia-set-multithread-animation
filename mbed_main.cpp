#include "mbed.h"
#include "messages.h"

DigitalOut myled(LED1);
Serial serial(SERIAL_TX, SERIAL_RX);
InterruptIn mybutton(USER_BUTTON);
Ticker ticker;

typedef struct complex_num {
    double real;
    double imag;
} ComplexNum;

void Tx_interrupt();
void Rx_interrupt();
bool send_buffer(const uint8_t* msg, unsigned int size);
bool receive_message(uint8_t *msg_buf, int size, int *len);
bool send_message(const message *msg, uint8_t *buf, int size);
uint8_t compute_iterations(ComplexNum Z0, ComplexNum C, uint8_t N);

#define BUF_SIZE 255

char tx_buffer[BUF_SIZE];
char rx_buffer[BUF_SIZE];

// pointers to the circular buffers
volatile int tx_in = 0;
volatile int tx_out = 0;
volatile int rx_in = 0;
volatile int rx_out = 0;

#define MESSAGE_SIZE sizeof(message)

bool abort_request = false;

void tick() {
    myled = !myled;
}

void pressed() {
    abort_request = true;
    return;
}

int main() {
    serial.baud(115200);
    serial.attach(&Rx_interrupt, Serial::RxIrq); // attach interrupt handler to receive data
    serial.attach(&Tx_interrupt, Serial::TxIrq); // attach interrupt handler to transmit data

    for (int i = 0; i < 10; i++) {
        myled = !myled;
        wait(0.15);
    }
    while (serial.readable())
        serial.getc();

    struct {
        uint8_t cid;
        uint8_t iter;
        ComplexNum Z; // complex number to compute
    } compute_data = { 0, 0, 0, 0 };

    struct {
        uint8_t cid; // chunk id
        ComplexNum start; // start coords
        uint8_t n_re; // number of cells in x-coords
        uint8_t n_im; // number of cells in y-coords
    } compute = { 0, 0, 0, 0, 0};

    struct {
        ComplexNum c;  // c constant in recursive equation
        ComplexNum inc;  // increment in complex number
        uint8_t n;    // number of iterations per each pixel
    } set_compute = { 0, 0, 0, 0, 0};

    message msg = { .type = MSG_STARTUP, .data.startup.message = {'P','R','G','-','S','E','M','0','1'}};
    uint8_t msg_buf[MESSAGE_SIZE];
    int msg_len;

    send_message(&msg, msg_buf, MESSAGE_SIZE);
    bool computing = false;
    float period = 0.2;
    mybutton.fall(&pressed);
    while (1) {
        if (abort_request) {
            if (computing) {  //abort computing
                msg.type = MSG_ABORT;
                send_message(&msg, msg_buf, MESSAGE_SIZE);
                computing = false;
                abort_request = false;
                ticker.detach();
                myled = 0;
            }
        }
        if (rx_in != rx_out) { // something is in the receive buffer
            if (receive_message(msg_buf, MESSAGE_SIZE, &msg_len)) {
                if (parse_message_buf(msg_buf, msg_len, &msg)) {
                    switch(msg.type) {
                        case MSG_GET_VERSION:
                            msg.type = MSG_VERSION;
                            msg.data.version.major  = 0;
                            msg.data.version.minor  = 1;
                            msg.data.version.patch  = 2;
                            send_message(&msg, msg_buf, MESSAGE_SIZE);
                            break;
                        case MSG_ABORT:
                            abort_request = true;
                            computing = false;
                            ticker.detach();
                            myled = 0;
                            break;
                        case MSG_SET_COMPUTE:
                            ticker.attach(tick, period);
                            set_compute.c.real = msg.data.set_compute.c_re;
                            set_compute.c.imag = msg.data.set_compute.c_im;
                            set_compute.inc.real = msg.data.set_compute.d_re;
                            set_compute.inc.imag = msg.data.set_compute.d_im;
                            set_compute.n = msg.data.set_compute.n;
                            msg.type = MSG_OK;
                            send_message(&msg, msg_buf, MESSAGE_SIZE);
                            break;
                        case MSG_COMPUTE:
                            ticker.attach(tick, period);
                            compute_data.cid = msg.data.compute.cid;
                            compute.start.real = msg.data.compute.re;
                            compute.start.imag = msg.data.compute.im;
                            compute.n_re = msg.data.compute.n_re;
                            compute.n_im = msg.data.compute.n_im;
                            compute_data.Z = compute.start;
                            computing = true;
                            msg.type = MSG_OK;
                            send_message(&msg, msg_buf, MESSAGE_SIZE);
                            break;
                    } // end switch
                } else { // message has not been parsed send error
                    msg.type = MSG_ERROR;
                    send_message(&msg, msg_buf, MESSAGE_SIZE);
                }
            } // end message received
        } else if (computing) {
            for (uint8_t i = 0; i < compute.n_re; ++i) {
                compute_data.Z.real += set_compute.inc.real;
                compute_data.Z.imag = compute.start.imag;
                for (uint8_t j = 0; j < compute.n_im; ++j) {
                    compute_data.iter = compute_iterations(compute_data.Z, set_compute.c, set_compute.n);
                    compute_data.Z.imag += set_compute.inc.imag;
                    msg.type = MSG_COMPUTE_DATA;
                    msg.data.compute_data.cid = compute_data.cid;
                    msg.data.compute_data.i_re = i;
                    msg.data.compute_data.i_im = j;
                    msg.data.compute_data.iter = compute_data.iter;
                    send_message(&msg, msg_buf, MESSAGE_SIZE);
                }
            }
            ticker.detach();
            myled = 0;
            msg.type = MSG_DONE;
            send_message(&msg, msg_buf, MESSAGE_SIZE);
            computing = false;
        } else {
            sleep(); // put the cpu to sleep mode, it will be wakeup on interrupt
        }
    } // end while (1)
}



bool send_message(const message *msg, uint8_t *buf, int size) {
    return fill_message_buf(msg, buf, MESSAGE_SIZE, &size)
           && send_buffer(buf, size);
}

void Tx_interrupt()
{
    // send a single byte as the interrupt is triggered on empty out buffer
    if (tx_in != tx_out) {
        serial.putc(tx_buffer[tx_out]);
        tx_out = (tx_out + 1) % BUF_SIZE;
    } else { // buffer sent out, disable Tx interrupt
        USART2->CR1 &= ~USART_CR1_TXEIE; // disable Tx interrupt
    }
    return;
}

void Rx_interrupt()
{
    // receive bytes and stop if rx_buffer is full
    while ((serial.readable()) && (((rx_in + 1) % BUF_SIZE) != rx_out)) {
        rx_buffer[rx_in] = serial.getc();
        rx_in = (rx_in + 1) % BUF_SIZE;
    }
    return;
}

bool send_buffer(const uint8_t* msg, unsigned int size)
{
    if (!msg && size == 0) {
        return false;    // size must be > 0
    }
    int i = 0;
    NVIC_DisableIRQ(USART2_IRQn); // start critical section for accessing global data
    USART2->CR1 |= USART_CR1_TXEIE; // enable Tx interrupt on empty out buffer
    bool empty = (tx_in == tx_out);
    while ( (i == 0) || i < size ) { //end reading when message has been read
        if ( ((tx_in + 1) % BUF_SIZE) == tx_out) { // needs buffer space
            NVIC_EnableIRQ(USART2_IRQn); // enable interrupts for sending buffer
            while (((tx_in + 1) % BUF_SIZE) == tx_out) {
                /// let interrupt routine empty the buffer
            }
            NVIC_DisableIRQ(USART2_IRQn); // disable interrupts for accessing global buffer
        }
        tx_buffer[tx_in] = msg[i];
        i += 1;
        tx_in = (tx_in + 1) % BUF_SIZE;
    } // send buffer has been put to tx buffer, enable Tx interrupt for sending it out
    USART2->CR1 |= USART_CR1_TXEIE; // enable Tx interrupt
    NVIC_EnableIRQ(USART2_IRQn); // end critical section
    return true;
}

bool receive_message(uint8_t *msg_buf, int size, int *len)
{
    bool ret = false;
    int i = 0;
    *len = 0; // message size
    NVIC_DisableIRQ(USART2_IRQn); // start critical section for accessing global data
    while ( ((i == 0) || (i != *len)) && i < size ) {
        if (rx_in == rx_out) { // wait if buffer is empty
            NVIC_EnableIRQ(USART2_IRQn); // enable interrupts for receing buffer
            while (rx_in == rx_out) { // wait of next character
            }
            NVIC_DisableIRQ(USART2_IRQn); // disable interrupts for accessing global buffer
        }
        uint8_t c = rx_buffer[rx_out];
        if (i == 0) { // message type
            if (get_message_size(c, len)) { // message type recognized
                msg_buf[i++] = c;
                ret = *len <= size; // msg_buffer must be large enough
            } else {
                ret = false;
                break; // unknown message
            }
        } else {
            msg_buf[i++] = c;
        }
        rx_out = (rx_out + 1) % BUF_SIZE;
    }
    NVIC_EnableIRQ(USART2_IRQn); // end critical section
    return ret;
}

uint8_t compute_iterations(ComplexNum Z0, ComplexNum C, uint8_t N) {
    ComplexNum Z = Z0;
    float a = Z.real;
    float b = Z.imag;

    for (int i = 0; i < N; ++i) {
        float aa = a * a;
        float bb = b * b;
        if (aa + bb > 4) {
            return (uint8_t) i;
        }
        float two_ab = 2 * a * b;
        a = aa - bb + C.real;
        b = two_ab + C.imag;
    }
    return N;
}