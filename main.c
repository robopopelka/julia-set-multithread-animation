#include <stdlib.h>
#include <stdbool.h>

#include <termios.h>
#include <unistd.h>  // for STDIN_FILENO

#include <pthread.h>

#include "prg_serial_nonblock.h"
#include "messages.h"
#include "event_queue.h"
#include "julia_set.h"
#include "xwin_sdl.h"
#include "gui.h"

#define SERIAL_READ_TIMOUT_MS 500 //timeout for reading from serial port
#define MSG_VERSION_LEN 4
#define MSG_COMPUTE_LEN 5
#define MSG_COMPUTE_DATA_LEN 6
#define ANIMATION_TIME 500

// shared data structure
typedef struct {
    bool quit;
    bool compute;
    bool animate;
    bool anim_stop;
    bool zoom;
    int fd; // serial port file descriptor
} data_t;

pthread_mutex_t mtx;
pthread_cond_t cond;

void call_termios(int reset);

void* input_thread(void*);
void* serial_rx_thread(void*); // serial receive buffer
void* compute_thread(void* d);

bool send_message(data_t *data, message *msg);
void print_help(void);

// - main ---------------------------------------------------------------------
int main(int argc, char *argv[])
{
    data_t data = { .quit = false, .compute = false, .animate = false, .anim_stop = false, .zoom = false, .fd = -1};
    const char *serial = argc > 1 ? argv[1] : "/dev/ttyACM0";
    data.fd = serial_open(serial);

    if (data.fd == -1) {
        fprintf(stderr, "ERROR: Cannot open serial port %s\n", serial);
        exit(100);
    }

    enum { INPUT, SERIAL_RX, COMPUTE_T, NUM_THREADS };
    const char *threads_names[] = { "Input", "Serial In", "Compute" };

    void* (*thr_functions[])(void*) = { input_thread, serial_rx_thread, compute_thread };

    pthread_t threads[NUM_THREADS];
    pthread_mutex_init(&mtx, NULL); // initialize mutex with default attributes
    pthread_cond_init(&cond, NULL); // initialize mutex with default attributes

    call_termios(0);

    for (int i = 0; i < NUM_THREADS; ++i) {
        int r = pthread_create(&threads[i], NULL, thr_functions[i], &data);
        fprintf(stderr, "INFO: Create thread '%s' %s\n", threads_names[i], ( r == 0 ? "OK" : "FAIL") );
    }

    message msg;
    bool quit = false;
    computauion_init();
    gui_init();
    while (!quit) {
        xwin_poll_events();
        event ev = queue_pop();
        if (ev.source == EV_KEYBOARD) {
            msg.type = MSG_NBR;
            // handle keyboard events
            switch(ev.type) {
                case EV_GET_VERSION: // prepare packet for get version
                    msg.type = MSG_GET_VERSION;
                    fprintf(stderr, "INFO: Get version requested\n");
                    break;
                case EV_QUIT:
                    if (is_computing()) {
                        msg.type = MSG_ABORT;
                    }
                    quit = true;
                    data.quit = true;
                    break;
                case EV_ABORT:
                    if (data.animate || data.zoom) {
                        data.anim_stop = true;
                    }
                    if (is_computing()) {
                        msg.type = MSG_ABORT;
                        fprintf(stderr, "INFO: Abort request sent\n\r");
                    } else {
                        fprintf(stderr, "WARN: Abort requested but it is not computing\n\r");
                    }
                    break;
                case EV_RESET_CHUNK:
                    fprintf(stderr, "INFO: Chunk reset request\n\r");
                    if (!is_computing()) {
                        reset_chunk();
                    } else {
                        fprintf(stderr, "WARN: Chunk reset request discarded, it is currently computing\n\r");
                    }
                    break;
                case EV_SET_COMPUTE:
                    if (set_compute(&msg)) {
                        fprintf(stderr, "INFO: Set new computation resolution %dx%d no. of chunks: %d\n",msg.data.set_compute.width, msg.data.set_compute.height, msg.data.set_compute.chunk_num);
                    } else {
                        fprintf(stderr, "WARN: New computation parameters requested but it is discarded due to on ongoing computation\n");
                    }
                    break;
                case EV_COMPUTE_CPU:
                    data.compute = true;
                    break;
                case EV_CLEAR_BUFFER:
                    gui_clear_buffer();
                    break;
                case EV_REFRESH:
                    gui_refresh();
                    break;
                case EV_COMPUTE:
                    enable_comp();
                    if (compute(&msg)) {
                        fprintf(stderr, "INFO: New computation chunk id: %d \n\r", msg.data.compute_data.cid);
                    } else {
                        fprintf(stderr, "WARN: New computation request fail\n\r");
                    }
                    break;
                case EV_ANIMATION:
                    data.animate = true;
                    break;
                case EV_ZOOM:
                    data.zoom = true;
                    break;
                default:
                    break;
            }
            if (msg.type != MSG_NBR) { // messge has been set
                if (!send_message(&data, &msg)) {
                    fprintf(stderr, "ERROR: send_message() does not send all bytes of the message!\n");
                }
            }
        } else if (ev.source == EV_NUCLEO) { // handle nucleo events
            if (ev.type == EV_SERIAL) {
                message *msg = ev.data.msg;
                switch (msg->type) {
                    case MSG_STARTUP:
                    {
                        char str[STARTUP_MSG_LEN+1];
                        for (int i = 0; i < STARTUP_MSG_LEN; ++i) {
                            str[i] = msg->data.startup.message[i];
                        }
                        str[STARTUP_MSG_LEN] = '\0';
                        fprintf(stderr, "INFO: Nucleo restarted - '%s'\n", str);
                    }
                        break;
                    case MSG_VERSION:
                        if (msg->data.version.patch > 0) {
                            fprintf(stderr, "INFO: Nucleo firmware ver. %d.%d-p%d\n", msg->data.version.major, msg->data.version.minor, msg->data.version.patch);
                        } else {
                            fprintf(stderr, "INFO: Nucleo firmware ver. %d.%d\n", msg->data.version.major, msg->data.version.minor);
                        }
                        break;
                    case MSG_ABORT:
                        abort_comp();
                        fprintf(stderr, "INFO: Abort from Nucleo\r\n");
                        break;
                    case MSG_ERROR:
                        fprintf(stderr, "WARN: Receive error from Nucleo\r\n");
                        break;
                    case MSG_OK:
                        fprintf(stderr, "INFO: Receive ok from Nucleo\r\n");
                        break;
                    case MSG_COMPUTE_DATA:
                        if (!is_abort()) {
                            update_data(&(msg->data.compute_data));
                        }
                        break;
                    case MSG_DONE:
                        gui_refresh();
                        if (!is_done()) {
                            event ev = {.source = EV_KEYBOARD, .type = EV_COMPUTE};
                            queue_push(ev);
                        } else {
                            fprintf(stderr, "INFO: Nucleo reports the computation is done computing: %d\n",is_computing());
                        }
                        break;
                    default:
                        fprintf(stderr, "ERROR: Unknown message type has been received 0x%x\n - '%c'\r", msg->type, msg->type);
                        break;
                }
                if (msg) {
                    free(msg);
                }
            } else {
                if(ev.type == EV_QUIT) {
                    quit = true;
                    data.quit = true;
                    break;
                }
            }
        }
    } // end main quit
    queue_cleanup(); // cleanup all events and free allocated memory for messages.
    while (data.animate || data.zoom) {}; // wait for current frame of the animation to finish computing
    computauion_cleanup();
    gui_cleanup();
    for (int i = 0; i < NUM_THREADS; ++i) {
        fprintf(stderr, "INFO: Call join to the thread %s\n", threads_names[i]);
        int r = pthread_join(threads[i], NULL);
        fprintf(stderr, "INFO: Joining the thread %s has been %s\n", threads_names[i], (r == 0 ? "OK" : "FAIL"));
    }
    serial_close(data.fd);
    call_termios(1); // restore terminal settings
    return EXIT_SUCCESS;
}

// - function -----------------------------------------------------------------
void call_termios(int reset)
{
    static struct termios tio, tioOld;
    tcgetattr(STDIN_FILENO, &tio);
    if (reset) {
        tcsetattr(STDIN_FILENO, TCSANOW, &tioOld);
    } else {
        tioOld = tio; //backup
        cfmakeraw(&tio);
        tio.c_oflag |= OPOST;
        tcsetattr(STDIN_FILENO, TCSANOW, &tio);
    }
}

// - function -----------------------------------------------------------------
void* input_thread(void* d)
{
    data_t *data = (data_t*)d;
    bool end = false;
    unsigned char c;
    event ev = { .source = EV_KEYBOARD };
    while (!end) {
        int r = serial_getc_timeout(0, SERIAL_READ_TIMOUT_MS, &c);
        if (r > 0) {
            ev.type = EV_TYPE_NUM;
            switch(c) {
                case 'g': // get version
                    ev.type = EV_GET_VERSION;
                    break;
                case 'q':
                    end = true;
                    break;
                case 'a':
                    ev.type = EV_ABORT;
                    break;
                case 'r':
                    ev.type = EV_RESET_CHUNK;
                    break;
                case 's':
                    ev.type = EV_SET_COMPUTE;
                    break;
                case 'l':
                    ev.type = EV_CLEAR_BUFFER;
                    break;
                case 'p':
                    ev.type = EV_REFRESH;
                    break;
                case 'c':
                    ev.type = EV_COMPUTE_CPU;
                    break;
                case '1':
                    ev.type = EV_COMPUTE;
                    break;
                case 'h':
                    print_help();
                    break;
                case 'm':
                    ev.type = EV_ANIMATION;
                    break;
                case 'b':
                    change_c();
                    break;
                case 'z':
                    ev.type = EV_ZOOM;
                    break;
                case 'o':
                    reset_c();
                    reset_range();
                    fprintf(stderr, "INFO: Default C constant and range values set\n");
                    break;
                default: // discard all other keys
                    break;
            }
            if (ev.type != EV_TYPE_NUM) { // new event
                queue_push(ev);
            }
        }
        pthread_mutex_lock(&mtx);
        end = end || data->quit; // check for quit
        pthread_mutex_unlock(&mtx);
    }
    ev.type = EV_QUIT;
    queue_push(ev);
    fprintf(stderr, "INFO: Exit input thead %p\n", (void*)pthread_self());
    return NULL;
}

// - function -----------------------------------------------------------------
void* serial_rx_thread(void* d)
{ // read bytes from the serial and puts the parsed message to the queue
    data_t *data = (data_t*)d;
    uint8_t msg_buf[sizeof(message)]; // maximal buffer for all possible messages defined in messages.h
    event ev = { .source = EV_NUCLEO, .type = EV_SERIAL, .data.msg = NULL };
    bool end = false;
    unsigned char c;
    unsigned char cc;
    int msg_size = 0;
    while (serial_getc_timeout(data->fd, SERIAL_READ_TIMOUT_MS, &c) > 0) {}; // discard garbage

    while (!end) {
        int r = serial_getc_timeout(data->fd, SERIAL_READ_TIMOUT_MS, &c);
        if (r > 0) { // character has been read
            message *msg_received = malloc(sizeof(message));
            if (!get_message_size(c, &msg_size)){
                fprintf(stderr, "ERROR: Unknown message type has been received 0x%x\n - '%c'\r", c, c);
                break;
            }
            msg_buf[0] = c;
            for (int i = 1; i < msg_size; i++) {
                serial_getc_timeout(data->fd, SERIAL_READ_TIMOUT_MS, &cc);
                msg_buf[i] = cc;
            }
            if (parse_message_buf(msg_buf, msg_size,msg_received)) {
                ev.data.msg = msg_received;
                queue_push(ev);
            } else {
                fprintf(stderr, "ERROR: Cannot parse message type %d\n\r", msg_buf[0]);
            }

        } else if (r == 0) { //read but nothing has been received

        } else {
            fprintf(stderr, "ERROR: Cannot receive data from the serial port\n");
            end = true;
        }
        pthread_mutex_lock(&mtx);
        end = end || data->quit; // check for quit
        pthread_mutex_unlock(&mtx);
    }
    ev.type = EV_QUIT;
    queue_push(ev);
    fprintf(stderr, "INFO: Exit serial_rx_thread %p\n", (void*)pthread_self());
    return NULL;
}

void* compute_thread(void* d) {
    data_t *data = (data_t *) d;
    bool end = false;
    bool compute = false;
    bool animate = false;
    bool zoom = false;
    bool anim_stop = false;
    int timer;
    while (!end) {
        pthread_mutex_lock(&mtx);
        compute = data->compute;
        animate = data->animate;
        zoom = data->zoom;
        anim_stop = data->anim_stop;
        pthread_mutex_unlock(&mtx);
        if (anim_stop){
            animate = false;
            zoom = false;
            timer = 0;
            pthread_mutex_lock(&mtx);
            data->animate = false;
            data->zoom = false;
            data->anim_stop = false;
            pthread_mutex_unlock(&mtx);
            fprintf(stderr, "INFO: Animation interrupted!\n");
        }
        if (compute){
            compute_fractal();
            fprintf(stderr, "INFO: Computation done!\n");
            pthread_mutex_lock(&mtx);
            data->compute = false;
            pthread_mutex_unlock(&mtx);
        } else if (animate) {   // shifting the C constant and repeatedly computing the fractal to animate
            if (timer < ANIMATION_TIME) {
                compute_fractal();
                shift_c();
                timer++;
            } else {
                timer = 0;
                fprintf(stderr, "INFO: Animation finished!\n");
                pthread_mutex_lock(&mtx);
                data->animate = false;
                pthread_mutex_unlock(&mtx);
            }
        } else if (zoom) {     // shifting the range of complex numbers to animate zoom
            if (timer < ANIMATION_TIME) {
                compute_fractal();
                shift_range();
                timer++;
            } else {
                timer = 0;
                fprintf(stderr, "INFO: Zoom animation finished!\n");
                pthread_mutex_lock(&mtx);
                data->zoom = false;
                pthread_mutex_unlock(&mtx);
            }
        }
        pthread_mutex_lock(&mtx);
        end = end || data->quit; // check for quit
        pthread_mutex_unlock(&mtx);
    }
    pthread_mutex_lock(&mtx);
    data->animate = false;
    data->zoom = false;
    pthread_mutex_unlock(&mtx);
    fprintf(stderr, "INFO: Exit compute_thread %p\n", (void *) pthread_self());
    return NULL;
}

// - function -----------------------------------------------------------------
bool send_message(data_t *data, message *msg)
{
    uint8_t msg_buf[sizeof(message)];
    int msg_len;
    fill_message_buf(msg, msg_buf, sizeof(msg_buf), &msg_len);
    int ret = write(data->fd, msg_buf, msg_len);
    return ret;
}

void print_help(void) {

    // Simple function to print all the possible letters to be pressed and their functions.
    fprintf(stderr, "*******************************************************************************\n");
    fprintf(stderr, "              * Overview of key commands *\n\r");
    fprintf(stderr, "  's' - send the parameters of a chunk to NUCLEO \n\r");
    fprintf(stderr, "  '1' - start the fractal computation on NUCLEO \n\r");
    fprintf(stderr, "  'c' - compute the fractal on the computer \n\r");
    fprintf(stderr, "  'a' - abort the computation \n\r");
    fprintf(stderr, "  'r' - reset the chunk ID (if the computation is not running)\n\r");
    fprintf(stderr, "  'l' - clear the buffer and the image \n\r");
    fprintf(stderr, "  'q' - quit \n\r");
    fprintf(stderr, "  'm' - animate the change in the fractal by changing C values \n\r");
    fprintf(stderr, "  'z' - animate the change in the fractal by changing the range values \n\r");
    fprintf(stderr, "  'b' - change the C constant value \n\r");
    fprintf(stderr, "  'o' - reset C constant and range to default \n\r");
    fprintf(stderr, "  'g' - request the current version of NUCLEO program \n\r");
    fprintf(stderr, "********************************************************************************\n");
}
