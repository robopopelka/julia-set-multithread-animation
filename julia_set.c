/*
Most of the code was heavily inspired by prof Faigl and his video tutorial
*/

#include "julia_set.h"
#include "gui.h"
#include <unistd.h>

#define NUMBER_OF_CHUNKS 100
#define DEFAULT_ITERATION 60
#define C_RE_DEFAULT -0.4
#define C_IM_DEFAULT 0.6
#define CHUNK_RE_DEFAULT 64
#define CHUNK_IM_DEFAULT 48
#define WINDOW_W_DEFAULT 640
#define WINDOW_H_DEFAULT 480
#define ANIMATION_SHIFT 0.003
#define DIV_TEST 4
#define RANGE_RE_MIN_DEFAULT -1.6
#define RANGE_RE_MAX_DEFAULT 1.6
#define RANGE_IM_MIN_DEFAULT -1.1
#define RANGE_IM_MAX_DEFAULT 1.1
#define ZOOM_SHIFT_RE 0.0032
#define ZOOM_SHIFT_IM 0.0022


struct {
    ComplexNum C;   // C constant
    ComplexNum range_max;
    ComplexNum range_min;
    ComplexNum increment;
    ComplexNum chunk;

    int n;  // number of iterations
    int grid_w;     // width and height of window
    int grid_h;
    int cur_x;      // cursor
    int cur_y;
    int nbr_chunks;     // number of chunks to compute
    int cid;        // chunk id

    uint8_t chunk_n_re;     // iteration values of a concrete pixel
    uint8_t chunk_n_im;

    uint8_t *grid;      // img buffer

    bool computing;
    bool abort;
    bool done;
    bool c_switch;
} comp = {
        .C.real = C_RE_DEFAULT,
        .C.imag = C_IM_DEFAULT,
        .n = DEFAULT_ITERATION,

        .range_min.real = RANGE_RE_MIN_DEFAULT,
        .range_max.real = RANGE_RE_MAX_DEFAULT,
        .range_min.imag = RANGE_IM_MIN_DEFAULT,
        .range_max.imag = RANGE_IM_MAX_DEFAULT,

        .grid = NULL,
        .grid_w = WINDOW_W_DEFAULT,
        .grid_h = WINDOW_H_DEFAULT,

        .chunk_n_re = CHUNK_RE_DEFAULT,
        .chunk_n_im = CHUNK_IM_DEFAULT,

        .computing = false,
        .done = false,
        .abort = false,
        .c_switch = false};


bool is_computing(void) { return comp.computing; }

bool is_done(void) { return comp.done; }

bool is_abort(void) { return comp.abort; }

void abort_comp(void) { comp.computing = false; comp.abort = true; }

void enable_comp(void) { comp.abort = false; }

void change_c(void) {
    if (comp.C.real == 0.4) {
        comp.c_switch = true;
    } else if (comp.C.real == -0.4) {
        comp.c_switch = false;
    }
    if (comp.c_switch) {
        comp.C.real -= 0.2;
        comp.C.imag += 0.3;
    } else {
        comp.C.real += 0.2;
        comp.C.imag -= 0.3;
    }
    fprintf(stderr, "INFO: Const C changed to: %f %f\n", comp.C.real, comp.C.imag);
}

void computauion_init(void) {
    comp.grid = my_alloc(comp.grid_w * comp.grid_h);
    compute_increment();
    comp.nbr_chunks = (comp.grid_w * comp.grid_h) / (comp.chunk_n_re * comp.chunk_n_im);
}

void compute_increment(void) {
    comp.increment.real = (comp.range_max.real - comp.range_min.real) / (double) comp.grid_w;
    comp.increment.imag = -(comp.range_max.imag - comp.range_min.imag) / (double) comp.grid_h;
}

void computauion_cleanup(void) {
    if (comp.grid) {
        free(comp.grid);
    }
    comp.grid = NULL;
}

bool set_compute(message *msg) {
    my_assert(msg != NULL, __func__, __LINE__, __FILE__);
    bool ret = !is_computing();
    if (ret) {
        compute_increment();
        msg->type = MSG_SET_COMPUTE;
        msg->data.set_compute.c_re = comp.C.real;
        msg->data.set_compute.c_im = comp.C.imag;
        msg->data.set_compute.d_re = comp.increment.real;
        msg->data.set_compute.d_im = comp.increment.imag;
        msg->data.set_compute.n = comp.n;
        msg->data.set_compute.chunk_num = NUMBER_OF_CHUNKS;
        msg->data.set_compute.height = comp.grid_h;
        msg->data.set_compute.width = comp.grid_w;
        comp.done = false;
    }
    return ret;
}

bool compute(message *msg) {
    if (!is_computing()) {
        comp.cid = 0;
        comp.computing = true;
        comp.cur_x = comp.cur_y = 0;
        comp.chunk.real = comp.range_min.real;
        comp.chunk.imag = comp.range_max.imag;
        msg->type = MSG_COMPUTE;
    } else {
        comp.cid += 1;
        if (comp.cid < comp.nbr_chunks)     // increase cid and continue untill reaching the quota
        {
            comp.cur_x += comp.chunk_n_re;
            comp.chunk.real += comp.chunk_n_re * comp.increment.real;
            if (comp.cur_x >= comp.grid_w) {
                comp.chunk.real = comp.range_min.real;
                comp.chunk.imag += comp.chunk_n_im * comp.increment.imag;
                comp.cur_x = 0;
                comp.cur_y += comp.chunk_n_im;
            }
            msg->type = MSG_COMPUTE;
        } else {};
    }
    if (comp.computing && msg->type == MSG_COMPUTE) {
        msg->data.compute.cid = comp.cid;
        msg->data.compute.re = comp.chunk.real;
        msg->data.compute.im = comp.chunk.imag;
        msg->data.compute.n_re = comp.chunk_n_re;
        msg->data.compute.n_im = comp.chunk_n_im;
    }

    return is_computing();
}

void update_data(const msg_compute_data *compute_data) {
    my_assert(compute_data != NULL, __func__, __LINE__, __FILE__);
    if (compute_data->cid == comp.cid) {
        const int idx = comp.cur_x + compute_data->i_re + (comp.cur_y + compute_data->i_im) * comp.grid_w;
        if (idx >= 0 && idx < (comp.grid_w * comp.grid_h)) {
            comp.grid[idx] = compute_data->iter;
        }
        if ((comp.cid + 1) >= comp.nbr_chunks && (compute_data->i_re + 1) == comp.chunk_n_re &&
            (compute_data->i_im + 1) == comp.chunk_n_im) {
            comp.done = true;
            comp.computing = false;
        }
    } else {
        fprintf(stderr, "ERROR: Received chunk with unexpected chunk id (cid)\n");
    }
}

void get_grid_size(int *w, int *h) {
    *w = comp.grid_w;
    *h = comp.grid_h;
}

void update_image(int w, int h, unsigned char *img) {
    my_assert(img && comp.grid && w == comp.grid_w && h == comp.grid_h, __func__, __LINE__, __FILE__);
    for (int i = 0; i < w * h; ++i) {
        const double t = 1. * comp.grid[i] / comp.n;
        *(img++) = 9 * (1 - t) * t * t * t * 255;
        *(img++) = 15 * (1 - t) * (1 - t) * t * t * 255;
        *(img++) = 8.5 * (1 - t) * (1 - t) * (1 - t) * t * 255;
    }
}

void compute_fractal() {
    ComplexNum shift;
    shift.real = (comp.range_min.real - comp.range_max.real) / (comp.grid_w);
    shift.imag = (comp.range_max.imag - comp.range_min.imag) / (comp.grid_h);
    ComplexNum Z;
    Z.imag = comp.range_min.imag;
    for (int j = 0; j < comp.grid_h; ++j) {
        Z.real = comp.range_max.real;
        for (int i = 0; i < comp.grid_w; ++i) {
            float a = Z.real;
            float b = Z.imag;
            int iter = 0;
            while (iter < comp.n) {
                float aa = a * a;
                float bb = b * b;
                if (aa + bb > DIV_TEST) {
                    break;
                }
                float two_ab = 2 * a * b;
                a = aa - bb + comp.C.real;
                b = two_ab + comp.C.imag;
                iter++;
            }
            comp.grid[i + j * comp.grid_w] = iter;
            Z.real += shift.real;
        }
        Z.imag += shift.imag;
    }
    gui_refresh();
}

void reset_buffer(int w, int h, unsigned char *img) {
    for (int i = 0; i <= w * h; ++i) {
        comp.grid[i] = 0;
    }
    for (int i = 0; i < w * h; ++i) {
        const double t = 1. * comp.grid[i] / (comp.n + 1.0);
        *(img++) = 9 * (1 - t) * t * t * t * 255;
        *(img++) = 15 * (1 - t) * (1 - t) * t * t * 255;
        *(img++) = 8.5 * (1 - t) * (1 - t) * (1 - t) * t * 255;
    }
}

void reset_chunk() {
    comp.cid = 0;
    comp.done = false;
    comp.cur_x = comp.cur_y = 0;
    comp.chunk.real = comp.range_min.real;
    comp.chunk.imag = comp.range_max.imag;
}

void shift_c(void) {
    comp.C.real -= ANIMATION_SHIFT;
    comp.C.imag -= ANIMATION_SHIFT;
}

void reset_c(void) {
    comp.C.real = C_RE_DEFAULT;
    comp.C.imag = C_IM_DEFAULT;
}

void shift_range(void) {
    comp.range_min.real += ZOOM_SHIFT_RE;
    comp.range_max.real -= ZOOM_SHIFT_RE;
    comp.range_min.imag += ZOOM_SHIFT_IM;
    comp.range_max.imag -= ZOOM_SHIFT_IM;
}

void reset_range(void) {
    comp.range_min.real = RANGE_RE_MIN_DEFAULT;
    comp.range_max.real = RANGE_RE_MAX_DEFAULT;
    comp.range_min.imag = RANGE_IM_MIN_DEFAULT;
    comp.range_max.imag = RANGE_IM_MAX_DEFAULT;
}

