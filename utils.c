#define ERROR_ASSERT 101
#define ERROR_MALLOC 102

#include "utils.h"

void my_assert(int r, const char *fcname, int line, const char *fname)
{
    if (!(r)) {
        fprintf(stderr, "ERROR: my_assert FAIL: %s() line %d in %s\n", fcname, line, fname);
        exit(ERROR_ASSERT);
    }
}

void *my_alloc(size_t size)
{
    void *ret = malloc(size);
    if (!ret)
    {
        fprintf(stderr, "ERROR: Cannot Malloc\r\n");
        exit(ERROR_MALLOC);
    }
    return ret;
}

