#ifndef GUI_H
#define GUI_H


void gui_init(void);
void gui_cleanup(void);

void gui_refresh(void);

void gui_clear_buffer(void);


#endif