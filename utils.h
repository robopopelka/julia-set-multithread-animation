#ifndef UTILS_H
#define UTILS_H

#include <stdbool.h>
#include <stdlib.h>

#include <stdio.h>

void my_assert(int r, const char *fcname, int line, const char *fname);
void* my_alloc(size_t size);
#endif